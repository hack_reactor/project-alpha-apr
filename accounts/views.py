from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from accounts.forms import loginForm, signupForm
from django.contrib.auth.models import User


# Create your views here.
def login_(request):
    if request.method == "POST":
        form = loginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = loginForm()
    context = {
        "form": form,
    }
    return render(request, "login.html", context)


def logout_(request):
    logout(request)
    return redirect("login")


def signup(request):
    if request.method == "POST":
        form = signupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                user.save()
                login(request, user)
                return redirect("list_projects")
    else:
        form = signupForm()
    context = {
        "form": form,
    }
    return render(request, "signup.html", context)
